Cambios en la version 3.0

24/05/2021
* Para facilitar la construcción se hace en una sola capa
* Se mueve el regulador SMD a la capa  Bottom
* Se ajustan los conectores J1, J4, J5, D3, F1, RV1 a paso estandar.
* Se reacomodan los componentes para el ajuste a una capa.

