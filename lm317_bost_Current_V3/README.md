# lm317_bost_Current_V3

Fuente regulada de voltaje variablecon bost de corriente.

Cuenta con un PCB sola capa aún cuando el regulador de voltaje es SMD en encapsulado D-PAK (TO-252).

Se eligió el encapsulado SMD para incentivar el uso de esta tecnología, ya que en la actualidad es fácil de conseguirlo en la CDMX y su colocación no requiere herramientas especializadas.

La terjeta luce de la siguiente forma

![kicad_top.png](img/01kicad.png)

![kicad_botom.png](img/02kicad.png)

![kicad_perspective.png](img/03kicad.png)

# Case

Se motiva a que la presentación final sea en un gabinete adecuado.

Puedes utilizar los archivos que se encuentran en [case](case) corte laser en MDF de 3mm.

Tu ensamble lucirá como el siguiente.

![img/photo_2020-12-11_09-15-28.jpg](img/photo_2020-12-11_09-15-28.jpg)

![img/photo_2020-12-11_09-15-33.jpg](img/photo_2020-12-11_09-15-33.jpg)

![/home/fenix/git/fuentes/lm317_bost_Current_V3/img/photo_2020-12-11_09-15-39.jpg](/home/fenix/git/fuentes/lm317_bost_Current_V3/img/photo_2020-12-11_09-15-39.jpg)
