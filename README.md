# Fuentes de alimentación

Circuitos varios para fuentes de alimentación.


Todos los circuitos se han desarrollado con herramientas libres:
* [KiCAD](kicad-pcb.com). Software libre para creación y edición
de circuitos esquemáticos y PCBs.

En cada carpeta se agregan los siguientes archivos:

* Archivos fuente para KiCAD.
* Archivos GERBER listos para producción.
* Archivos PDF de cada capa.
* Archivos PNG del PCB en 3D.

# Directorios

Se recomienda utilizar siempre las versiones más recientes.

## [lm317_bost_Current_V3](lm317_bost_Current_V3)
* Fuente regulada de voltaje variable.
* PCB de una sola capa.
* Incluye un componente SMD para incentivar el uso de esta tecnología con un componente de fácil colocación.
* Regulador LM317 en montaje superficial encapsulado D-PAK ([TO-252](https://www.vishay.com/docs/95016/dpak252a.pdf)).

## simulacion/[sim_lm317_high_current_V2](simulacion/sim_lm317_high_current_V2)
* Simulación en KiCAD para el LM317.
* Consulta el directorio para instrucciones de simulación.

# Git

Puedes clonar el repositorio con

	$ git clone https://gitlab.com/dpw19/fuentes.git

Son bienvenidas todas las aportaciones.

# Licencias
Todos los diseños puede ser utilizados libremente para su reproducción,
uso o adaptación en los términos de las licencias, Creative Commons y
GNU/GPL

## Creatve Commons
Para toda la documentación con las siguientes atribuciones

* BY Citar la Autoría
* SA Compartir Igual

# Deslinde de responsabilidad

Todas los diseños se realizan con las mejores intenciones y prácticas para que sean funcionales. La información publicada en este sitio funcionó al autor y no por ello garantiza que le funcione o deba funcional a otras personas, por tal motivo se deslinda de cualquier daño directo o indirecto, incidental o subsecuentes a la vida y/o equipos por el uso de la información contenida en este repositorio.
