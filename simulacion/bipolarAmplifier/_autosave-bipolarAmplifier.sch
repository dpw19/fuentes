EESchema Schematic File Version 5
EELAYER 43 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 5150 3800
Connection ~ 5450 2900
Connection ~ 5750 3550
Connection ~ 5750 4550
Connection ~ 6600 3550
Wire Wire Line
	1800 3450 1800 3300
Wire Wire Line
	1800 4050 1800 4200
Wire Wire Line
	2850 3300 2850 3450
Wire Wire Line
	2850 4200 2850 4050
Wire Wire Line
	4100 3800 4250 3800
Wire Wire Line
	4750 3800 5150 3800
Wire Wire Line
	5150 2900 5150 3150
Wire Wire Line
	5150 2900 5450 2900
Wire Wire Line
	5150 3650 5150 3800
Wire Wire Line
	5150 3800 5150 3950
Wire Wire Line
	5150 3800 5450 3800
Wire Wire Line
	5150 4550 5150 4450
Wire Wire Line
	5450 2750 5450 2900
Wire Wire Line
	5450 2900 5750 2900
Wire Wire Line
	5750 3000 5750 2900
Wire Wire Line
	5750 3500 5750 3550
Wire Wire Line
	5750 3550 5750 3600
Wire Wire Line
	5750 3550 5950 3550
Wire Wire Line
	5750 4000 5750 4550
Wire Wire Line
	5750 4550 5150 4550
Wire Wire Line
	5750 4550 5750 4750
Wire Wire Line
	6450 3550 6600 3550
Wire Wire Line
	6600 3550 6700 3550
Wire Wire Line
	6600 3850 6600 3550
Wire Wire Line
	6600 4350 6600 4550
Wire Wire Line
	6600 4550 5750 4550
Text Notes 1750 4550 0    50   ~ 0
.tran 100u 10m
Text GLabel 1800 3300 1    50   Input ~ 0
vcc
Text GLabel 2850 3300 1    50   Input ~ 0
in
Text GLabel 4100 3800 0    50   Input ~ 0
in
Text GLabel 5450 2750 1    50   Input ~ 0
vcc
Text GLabel 6700 3550 2    50   Input ~ 0
out
$Comp
L bipolarAmplifier-rescue:0-pspice #GND01
U 1 1 5F99FDA4
P 1800 4200
F 0 "#GND01" H 1800 4100 50  0001 C CNN
F 1 "0" H 1800 4289 50  0000 C CNN
F 2 "" H 1800 4200 50  0001 C CNN
F 3 "~" H 1800 4200 50  0001 C CNN
	1    1800 4200
	1    0    0    -1  
$EndComp
$Comp
L bipolarAmplifier-rescue:0-pspice #GND02
U 1 1 5F9A086E
P 2850 4200
F 0 "#GND02" H 2850 4100 50  0001 C CNN
F 1 "0" H 2850 4289 50  0000 C CNN
F 2 "" H 2850 4200 50  0001 C CNN
F 3 "~" H 2850 4200 50  0001 C CNN
	1    2850 4200
	1    0    0    -1  
$EndComp
$Comp
L bipolarAmplifier-rescue:0-pspice #GND03
U 1 1 5F998BE5
P 5750 4750
F 0 "#GND03" H 5750 4650 50  0001 C CNN
F 1 "0" H 5750 4839 50  0000 C CNN
F 2 "" H 5750 4750 50  0001 C CNN
F 3 "~" H 5750 4750 50  0001 C CNN
	1    5750 4750
	1    0    0    -1  
$EndComp
$Comp
L bipolarAmplifier-rescue:R-pspice R1
U 1 1 5F9AE001
P 5150 3400
F 0 "R1" H 5218 3446 50  0000 L CNN
F 1 "68k" H 5218 3355 50  0000 L CNN
F 2 "" H 5150 3400 50  0001 C CNN
F 3 "~" H 5150 3400 50  0001 C CNN
	1    5150 3400
	1    0    0    -1  
$EndComp
$Comp
L bipolarAmplifier-rescue:R-pspice R2
U 1 1 5F9AA90B
P 5150 4200
F 0 "R2" H 5218 4246 50  0000 L CNN
F 1 "10k" H 5218 4155 50  0000 L CNN
F 2 "" H 5150 4200 50  0001 C CNN
F 3 "~" H 5150 4200 50  0001 C CNN
	1    5150 4200
	1    0    0    -1  
$EndComp
$Comp
L bipolarAmplifier-rescue:R-pspice R3
U 1 1 5F9AEF53
P 5750 3250
F 0 "R3" H 5818 3296 50  0000 L CNN
F 1 "10k" H 5818 3205 50  0000 L CNN
F 2 "" H 5750 3250 50  0001 C CNN
F 3 "~" H 5750 3250 50  0001 C CNN
	1    5750 3250
	1    0    0    -1  
$EndComp
$Comp
L bipolarAmplifier-rescue:R-pspice R4
U 1 1 5F9AF47D
P 6600 4100
F 0 "R4" H 6668 4146 50  0000 L CNN
F 1 "100k" H 6668 4055 50  0000 L CNN
F 2 "" H 6600 4100 50  0001 C CNN
F 3 "~" H 6600 4100 50  0001 C CNN
	1    6600 4100
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC546 Q1
U 1 1 5F997BEB
P 5650 3800
F 0 "Q1" H 5841 3846 50  0000 L CNN
F 1 "BC546" H 5841 3755 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 5850 3725 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC547.pdf" H 5650 3800 50  0001 L CNN
F 4 "Q" H 5650 3800 50  0001 C CNN "Spice_Primitive"
F 5 "BC546" H 5650 3800 50  0001 C CNN "Spice_Model"
F 6 "Y" H 5650 3800 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "model/bc546.lib" H 5650 3800 50  0001 C CNN "Spice_Lib_File"
F 8 "3 2 1" H 5650 3800 50  0001 C CNN "Spice_Node_Sequence"
	1    5650 3800
	1    0    0    -1  
$EndComp
$Comp
L bipolarAmplifier-rescue:C-pspice C1
U 1 1 5F9B1456
P 4500 3800
F 0 "C1" V 4185 3800 50  0000 C CNN
F 1 "10u" V 4276 3800 50  0000 C CNN
F 2 "" H 4500 3800 50  0001 C CNN
F 3 "~" H 4500 3800 50  0001 C CNN
	1    4500 3800
	0    1    1    0   
$EndComp
$Comp
L bipolarAmplifier-rescue:C-pspice C2
U 1 1 5F9B0EEF
P 6200 3550
F 0 "C2" V 5885 3550 50  0000 C CNN
F 1 "10u" V 5976 3550 50  0000 C CNN
F 2 "" H 6200 3550 50  0001 C CNN
F 3 "~" H 6200 3550 50  0001 C CNN
	1    6200 3550
	0    1    1    0   
$EndComp
$Comp
L bipolarAmplifier-rescue:VSOURCE-pspice Vcc1
U 1 1 5F99F852
P 1800 3750
F 0 "Vcc1" H 2028 3796 50  0000 L CNN
F 1 "5" H 2028 3705 50  0000 L CNN
F 2 "" H 1800 3750 50  0001 C CNN
F 3 "~" H 1800 3750 50  0001 C CNN
	1    1800 3750
	1    0    0    -1  
$EndComp
$Comp
L bipolarAmplifier-rescue:VSOURCE-pspice Vin1
U 1 1 5F99F43A
P 2850 3750
F 0 "Vin1" H 3078 3796 50  0000 L CNN
F 1 "sin(0 1m 500)" H 3078 3705 50  0000 L CNN
F 2 "" H 2850 3750 50  0001 C CNN
F 3 "~" H 2850 3750 50  0001 C CNN
	1    2850 3750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
