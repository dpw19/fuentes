# Simulación del regulador de voltaje LM317 con bost de corriente

Para ver la simulacion en KiCAD sigue los siguientes pasos:
1. Abre el proyecto en KiCAD
1. Abre el esquemático
1. Herramientas -> Simulador
1. Clic en Ejecutar Simulación
1. Clic en Añadir Señales
1. Elegir las señales V(Vris) y V(Vreg)


# Resistencia de carga

Se espera que el valor de V(Vreg) se mantenga aún cuando la resistencia de carga sea muy pequeña.

Puedes variar el valor de RL de la siguiente forma
1. Clic en ajustar
1. Clic en la resistencia RL1
1. En la ventana de simulacion cambia los rangos y valor de la resistencia.
1. Observa lo que sucede con el voltaje V(Vreg).

# Bost de corriente

Elimina R3 y Q1 y realiza la misma prueba.
