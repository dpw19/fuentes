# FUente de Voltaje LM317 Bost Current V1

**Nota: Se sugiere utilizar la versión V2.**

Esta fuente se ha probado con exito, sin embargo tiene algunas
dificultades que complican su manufactura casera.

1. El discipador se debe aislar del PCB para no generar cortos
1. El conector de salida está en un liugar incomodo si se utilizan bornes
con sujeción de tornillos
1. No tiene proteccion contra cortos circuitos más alla del fusible en el
secundario del transformador.


