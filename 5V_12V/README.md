# Fuente regulada de voltaje fijo de 12V y 5V

Todos los componentes son pasantes

La tarjeta luce de la siguiente forma

![5v_12v_top.png](img/5v_12v_top.png)
![5v_12v_bot.png](img/5v_12v_bot.png)
![5v_12v_persp.png](img/5v_12v_persp.png)

# Case

Se motiva a que la presentación final sea en un gabinete adecuado.

Puedes utilizar los archivos que se encuentran en [case](case) para ajustarlo
a las necesidades de la fuente.

Se sugiere MDF de 3mm en corte láser.

# Lista de materiales

La lista de materiales interactiva iBOM se encuentra en el archivo [bom/ibom.html](bom/ibom.html),
puedes visualizarlo con cualquier navegador web moderno.
