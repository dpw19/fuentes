EESchema Schematic File Version 5
EELAYER 43 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 4500 2650
Connection ~ 4900 2650
Connection ~ 6530 2730
Connection ~ 6530 4680
Connection ~ 8110 2730
Connection ~ 8110 4680
Wire Wire Line
	2950 2650 3050 2650
Wire Wire Line
	2950 2750 2950 2650
Wire Wire Line
	3250 3650 3350 3650
Wire Wire Line
	3250 3750 3350 3750
Wire Wire Line
	3250 4900 3350 4900
Wire Wire Line
	3260 4260 3360 4260
Wire Wire Line
	3850 3250 3950 3250
Wire Wire Line
	3900 2650 3650 2650
Wire Wire Line
	3950 2950 3850 2950
Wire Wire Line
	4200 2650 4500 2650
Wire Wire Line
	4500 2650 4900 2650
Wire Wire Line
	4500 2750 4500 2650
Wire Wire Line
	4500 3050 4500 3150
Wire Wire Line
	4900 2650 4900 2750
Wire Wire Line
	4900 2650 5100 2650
Wire Wire Line
	4900 3150 4900 3050
Wire Wire Line
	6530 2730 6380 2730
Wire Wire Line
	6530 2730 7230 2730
Wire Wire Line
	6530 2910 6530 2730
Wire Wire Line
	6530 3340 6530 3210
Wire Wire Line
	6530 4680 6380 4680
Wire Wire Line
	6530 4680 7230 4680
Wire Wire Line
	6530 4860 6530 4680
Wire Wire Line
	6530 5290 6530 5160
Wire Wire Line
	7530 3030 7530 3340
Wire Wire Line
	7530 4980 7530 5290
Wire Wire Line
	7830 2730 8110 2730
Wire Wire Line
	7830 4680 8110 4680
Wire Wire Line
	8110 2730 8420 2730
Wire Wire Line
	8110 2980 8110 2730
Wire Wire Line
	8110 3280 8110 3330
Wire Wire Line
	8110 4680 8420 4680
Wire Wire Line
	8110 4930 8110 4680
Wire Wire Line
	8110 5230 8110 5280
Wire Notes Line style solid
	2700 1800 2700 6100
Wire Notes Line style solid
	2700 1800 9350 1800
Wire Notes Line style solid
	2700 6100 9350 6100
Wire Notes Line style solid
	4150 3400 4150 6100
Wire Notes Line style solid
	4150 4600 5500 4600
Wire Notes Line style solid
	5500 1800 5500 6100
Wire Notes Line style solid
	5500 3400 2700 3400
Wire Notes Line style solid
	5500 5350 4150 5350
Wire Notes Line style solid
	9350 1800 9350 6100
Text Notes 2800 1600 0    50   ~ 0
Fuente Regulada de Voltaje Variable\n12v a 3 A
Text Notes 3050 2100 0    50   ~ 0
Transformador\n120VAC a 12VAC\n3A
Text Notes 4750 2500 0    50   ~ 0
Vris = 12*sqt(2)\nVris = 17 VDC
Text GLabel 3250 3650 0    50   Input ~ 0
AC1
Text GLabel 3250 3750 0    50   Input ~ 0
AC2
Text GLabel 3250 4900 0    50   Input ~ 0
V5
Text GLabel 3260 4260 0    50   Input ~ 0
V12
Text GLabel 3350 2350 1    50   Input ~ 0
AC2
Text GLabel 3350 2950 3    50   Input ~ 0
AC1
Text GLabel 3850 2950 0    50   Input ~ 0
AC1
Text GLabel 3850 3250 0    50   Input ~ 0
AC2
Text GLabel 4750 3650 1    50   Input ~ 0
Vris
Text GLabel 5020 5000 3    50   Input ~ 0
Vris
Text GLabel 5100 2650 2    50   Input ~ 0
Vris
Text GLabel 6380 2730 0    50   Input ~ 0
Vris
Text GLabel 6380 4680 0    50   Input ~ 0
Vris
Text GLabel 8420 2730 2    50   Input ~ 0
V12
Text GLabel 8420 4680 2    50   Input ~ 0
V5
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5FA94E1A
P 4480 4970
F 0 "#FLG01" H 4480 5045 50  0001 C CNN
F 1 "PWR_FLAG" H 4480 5143 50  0000 C CNN
F 2 "" H 4480 4970 50  0001 C CNN
F 3 "~" H 4480 4970 50  0001 C CNN
	1    4480 4970
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 00000000
P 5020 5000
F 0 "#FLG02" H 5020 5075 50  0001 C CNN
F 1 "PWR_FLAG" H 5020 5173 50  0000 C CNN
F 2 "" H 5020 5000 50  0001 C CNN
F 3 "~" H 5020 5000 50  0001 C CNN
	1    5020 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5FA7B5D0
P 2950 2750
F 0 "#PWR01" H 2950 2500 50  0001 C CNN
F 1 "GND" H 2955 2577 50  0000 C CNN
F 2 "" H 2950 2750 50  0001 C CNN
F 3 "" H 2950 2750 50  0001 C CNN
	1    2950 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5FAEBE27
P 3350 5000
F 0 "#PWR0101" H 3350 4750 50  0001 C CNN
F 1 "GND" H 3355 4827 50  0000 C CNN
F 2 "" H 3350 5000 50  0001 C CNN
F 3 "" H 3350 5000 50  0001 C CNN
	1    3350 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 00000000
P 3360 4360
F 0 "#PWR010" H 3360 4110 50  0001 C CNN
F 1 "GND" H 3365 4187 50  0000 C CNN
F 2 "" H 3360 4360 50  0001 C CNN
F 3 "" H 3360 4360 50  0001 C CNN
	1    3360 4360
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5FAA4A10
P 4480 4970
F 0 "#PWR013" H 4480 4720 50  0001 C CNN
F 1 "GND" H 4485 4797 50  0000 C CNN
F 2 "" H 4480 4970 50  0001 C CNN
F 3 "" H 4480 4970 50  0001 C CNN
	1    4480 4970
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5FA7E608
P 4500 3150
F 0 "#PWR02" H 4500 2900 50  0001 C CNN
F 1 "GND" H 4505 2977 50  0000 C CNN
F 2 "" H 4500 3150 50  0001 C CNN
F 3 "" H 4500 3150 50  0001 C CNN
	1    4500 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5FAA86A4
P 4750 4250
F 0 "#PWR09" H 4750 4000 50  0001 C CNN
F 1 "GND" H 4755 4077 50  0000 C CNN
F 2 "" H 4750 4250 50  0001 C CNN
F 3 "" H 4750 4250 50  0001 C CNN
	1    4750 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5FA7E877
P 4900 3150
F 0 "#PWR03" H 4900 2900 50  0001 C CNN
F 1 "GND" H 4905 2977 50  0000 C CNN
F 2 "" H 4900 3150 50  0001 C CNN
F 3 "" H 4900 3150 50  0001 C CNN
	1    4900 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5FAA36E5
P 6530 3340
F 0 "#PWR04" H 6530 3090 50  0001 C CNN
F 1 "GND" H 6535 3167 50  0000 C CNN
F 2 "" H 6530 3340 50  0001 C CNN
F 3 "" H 6530 3340 50  0001 C CNN
	1    6530 3340
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 00000000
P 6530 5290
F 0 "#PWR07" H 6530 5040 50  0001 C CNN
F 1 "GND" H 6535 5117 50  0000 C CNN
F 2 "" H 6530 5290 50  0001 C CNN
F 3 "" H 6530 5290 50  0001 C CNN
	1    6530 5290
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5FA868B4
P 7530 3340
F 0 "#PWR05" H 7530 3090 50  0001 C CNN
F 1 "GND" H 7535 3167 50  0000 C CNN
F 2 "" H 7530 3340 50  0001 C CNN
F 3 "" H 7530 3340 50  0001 C CNN
	1    7530 3340
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 00000000
P 7530 5290
F 0 "#PWR08" H 7530 5040 50  0001 C CNN
F 1 "GND" H 7535 5117 50  0000 C CNN
F 2 "" H 7530 5290 50  0001 C CNN
F 3 "" H 7530 5290 50  0001 C CNN
	1    7530 5290
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5FAA30E4
P 8110 3330
F 0 "#PWR06" H 8110 3080 50  0001 C CNN
F 1 "GND" H 8115 3157 50  0000 C CNN
F 2 "" H 8110 3330 50  0001 C CNN
F 3 "" H 8110 3330 50  0001 C CNN
	1    8110 3330
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 00000000
P 8110 5280
F 0 "#PWR014" H 8110 5030 50  0001 C CNN
F 1 "GND" H 8115 5107 50  0000 C CNN
F 2 "" H 8110 5280 50  0001 C CNN
F 3 "" H 8110 5280 50  0001 C CNN
	1    8110 5280
	1    0    0    -1  
$EndComp
$Comp
L Device:Fuse F1
U 1 1 5FBAE419
P 4050 2650
F 0 "F1" V 3853 2650 50  0000 C CNN
F 1 "Fuse" V 3944 2650 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-396_A-41791-0002_1x02_P3.96mm_Vertical" V 3980 2650 50  0001 C CNN
F 3 "~" H 4050 2650 50  0001 C CNN
	1    4050 2650
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5FB54A4E
P 4450 5600
F 0 "H1" H 4550 5646 50  0000 L CNN
F 1 "M3" H 4550 5555 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 4450 5600 50  0001 C CNN
F 3 "~" H 4450 5600 50  0001 C CNN
	1    4450 5600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5FB55141
P 4450 5850
F 0 "H3" H 4550 5896 50  0000 L CNN
F 1 "M3" H 4550 5805 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 4450 5850 50  0001 C CNN
F 3 "~" H 4450 5850 50  0001 C CNN
	1    4450 5850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5FB5EC72
P 5100 5600
F 0 "H2" H 5200 5646 50  0000 L CNN
F 1 "M3" H 5200 5555 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 5100 5600 50  0001 C CNN
F 3 "~" H 5100 5600 50  0001 C CNN
	1    5100 5600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5FB5EC78
P 5100 5850
F 0 "H4" H 5200 5896 50  0000 L CNN
F 1 "M3" H 5200 5805 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 5100 5850 50  0001 C CNN
F 3 "~" H 5100 5850 50  0001 C CNN
	1    5100 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5FAA82A3
P 4750 3800
F 0 "R4" H 4820 3846 50  0000 L CNN
F 1 "1k" H 4820 3755 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4680 3800 50  0001 C CNN
F 3 "~" H 4750 3800 50  0001 C CNN
	1    4750 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5FB7E19D
P 4750 4100
F 0 "D3" V 4789 3982 50  0000 R CNN
F 1 "LED" V 4698 3982 50  0000 R CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 4750 4100 50  0001 C CNN
F 3 "~" H 4750 4100 50  0001 C CNN
	1    4750 4100
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5FA79363
P 3550 3650
F 0 "J1" H 3630 3642 50  0000 L CNN
F 1 "12VAC" H 3630 3551 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-396_A-41791-0002_1x02_P3.96mm_Vertical" H 3550 3650 50  0001 C CNN
F 3 "~" H 3550 3650 50  0001 C CNN
	1    3550 3650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J5
U 1 1 5FAEBE20
P 3550 4900
F 0 "J5" H 3630 4892 50  0000 L CNN
F 1 "5V" H 3630 4801 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-396_A-41791-0002_1x02_P3.96mm_Vertical" H 3550 4900 50  0001 C CNN
F 3 "~" H 3550 4900 50  0001 C CNN
	1    3550 4900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 00000000
P 3560 4260
F 0 "J2" H 3640 4252 50  0000 L CNN
F 1 "12V" H 3640 4161 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-396_A-41791-0002_1x02_P3.96mm_Vertical" H 3560 4260 50  0001 C CNN
F 3 "~" H 3560 4260 50  0001 C CNN
	1    3560 4260
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5FB3343A
P 3950 3100
F 0 "C9" H 4065 3146 50  0000 L CNN
F 1 "0.1u/250V" H 4065 3055 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L10.0mm_W5.0mm_P5.00mm_P7.50mm" H 3988 2950 50  0001 C CNN
F 3 "~" H 3950 3100 50  0001 C CNN
	1    3950 3100
	1    0    0    -1  
$EndComp
$Comp
L kicad-rescue:CP-Device C1
U 1 1 5FA7CCD6
P 4500 2900
F 0 "C1" H 4618 2946 50  0000 L CNN
F 1 "2200" H 4618 2855 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 4538 2750 50  0001 C CNN
F 3 "~" H 4500 2900 50  0001 C CNN
	1    4500 2900
	1    0    0    -1  
$EndComp
$Comp
L kicad-rescue:CP-Device C2
U 1 1 5FA7D3E9
P 4900 2900
F 0 "C2" H 5018 2946 50  0000 L CNN
F 1 "2200" H 5018 2855 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 4938 2750 50  0001 C CNN
F 3 "~" H 4900 2900 50  0001 C CNN
	1    4900 2900
	1    0    0    -1  
$EndComp
$Comp
L kicad-rescue:CP-Device C3
U 1 1 5FAA1279
P 6530 3060
F 0 "C3" H 6648 3106 50  0000 L CNN
F 1 "10u" H 6648 3015 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 6568 2910 50  0001 C CNN
F 3 "~" H 6530 3060 50  0001 C CNN
	1    6530 3060
	1    0    0    -1  
$EndComp
$Comp
L kicad-rescue:CP-Device C4
U 1 1 00000000
P 6530 5010
F 0 "C4" H 6648 5056 50  0000 L CNN
F 1 "10u" H 6648 4965 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 6568 4860 50  0001 C CNN
F 3 "~" H 6530 5010 50  0001 C CNN
	1    6530 5010
	1    0    0    -1  
$EndComp
$Comp
L kicad-rescue:CP-Device C5
U 1 1 5FAA21F2
P 8110 3130
F 0 "C5" H 8228 3176 50  0000 L CNN
F 1 "10u" H 8228 3085 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 8148 2980 50  0001 C CNN
F 3 "~" H 8110 3130 50  0001 C CNN
	1    8110 3130
	1    0    0    -1  
$EndComp
$Comp
L kicad-rescue:CP-Device C7
U 1 1 00000000
P 8110 5080
F 0 "C7" H 8228 5126 50  0000 L CNN
F 1 "10u" H 8228 5035 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 8148 4930 50  0001 C CNN
F 3 "~" H 8110 5080 50  0001 C CNN
	1    8110 5080
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM7812_TO220 U1
U 1 1 00000000
P 7530 2730
F 0 "U1" H 7530 3010 50  0000 C CNN
F 1 "LM7812" H 7530 2910 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7530 2955 50  0001 C CIN
F 3 "https://www.onsemi.cn/PowerSolutions/document/MC7800-D.PDF" H 7530 2680 50  0001 C CNN
	1    7530 2730
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM7805_TO220 U2
U 1 1 00000000
P 7530 4680
F 0 "U2" H 7530 4960 50  0000 C CNN
F 1 "LM7805" H 7530 4860 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7530 4905 50  0001 C CIN
F 3 "https://www.onsemi.cn/PowerSolutions/document/MC7800-D.PDF" H 7530 4630 50  0001 C CNN
	1    7530 4680
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Bridge_+AA- D2
U 1 1 00000000
P 3350 2650
F 0 "D2" H 3920 2737 50  0000 C CNN
F 1 "KBP206" H 3060 2860 50  0000 C CNN
F 2 "Diode_THT:Diode_Bridge_Vishay_KBPM" H 3350 2650 50  0001 C CNN
F 3 "~" H 3350 2650 50  0001 C CNN
	1    3350 2650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
